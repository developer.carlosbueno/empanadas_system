const User = require('../models/user')
const { success, fail } = require('../utils/response')
const generateToken = require('../utils/generateToken')

const login = async (req, res) => {
  const { username, password } = req.body

  try {
    let user = await User.findOne({ username })
    if (user && (await user.matchPassword(password))) {
      return success(res, 200, {
        _id: user._id,
        username: user.username,
        name: user.name,
        position: user.position,
        isAdmin: user.isAdmin,
        token: generateToken(user._id),
      })
    }
    fail(res, 400, 'Usuario o contraseña incorrecto')
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const register = async (req, res) => {
  const { username, password, name, position, number, isAdmin } = req.body

  try {
    let user = await User.findOne({ username })
    if (user) return success(res, 400, 'ya existe este usuario.')

    let newUser = await User.create({
      name,
      position,
      number,
      isAdmin,
      username,
      password,
    })

    success(res, 201, newUser)
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const getUsers = async (req, res) => {
  try {
    let users = await User.find({})
    success(res, 200, users)
  } catch (e) {
    fail(res, 500, e.message)
  }
}

const updateUser = async (req, res) => {
  const { name, number, isAdmin, position, username, password } = req.body

  try {
    let user = await User.findById(req.params.id)
    user.name = name
    user.number = number
    user.isAdmin = isAdmin
    user.position = position
    user.username = username
    user.password = password
    await user.save()
    success(res, 200, user)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

module.exports = {
  login,
  register,
  getUsers,
  updateUser,
}
