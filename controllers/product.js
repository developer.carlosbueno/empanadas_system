const Product = require('../models/product')
const { success, fail } = require('../utils/response')

const getAll = async (req, res) => {
  try {
    const products = await Product.find({})
    success(res, 200, products)
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const getOne = async (req, res) => {
  try {
    const product = await Product.findById(req.params.id)
    success(res, 200, product)
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const insert = async (req, res) => {
  const { name, price, category } = req.body

  try {
    const newProduct = await Product.create({ name, price, category })
    success(res, 201, newProduct)
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const updateProduct = async (req, res) => {
  const { name, price } = req.body

  try {
    const product = await Product.findById(req.params.id)
    product.name = name
    product.price = price
    let updatedProduct = await product.save()
    success(res, 200, updatedProduct)
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const deleteProduct = async (req, res) => {
  try {
    let deletedProduct = await Product.deleteOne({ _id: req.params.id })
    success(res, 200, deletedProduct)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

module.exports = {
  getAll,
  getOne,
  insert,
  updateProduct,
  deleteProduct,
}
