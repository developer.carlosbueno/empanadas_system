const Cuadre = require('../models/cuadre')
const { success, fail } = require('../utils/response')

const createCuadre = async (req, res) => {
  const { entryMoney } = req.body

  try {
    const cuadre = Cuadre.create({
      user: req.user._id,
      entryMoney,
    })
    success(res, 201, cuadre)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const getCuadres = async (req, res) => {
  try {
    let cuadres = await Cuadre.find({})
      .populate({
        path: 'user',
        select: '-password -number',
      })
      .populate('orders')
    success(res, 200, cuadres)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const getMyCuadre = async (req, res) => {
  let now = new Date()
  var startOfToday = new Date(now.getFullYear(), now.getMonth(), now.getDate())
  try {
    let cuadre = await Cuadre.findOne({
      user: req.params.id,
      createdAt: {
        $gte: startOfToday,
      },
    })
      .populate({
        path: 'user',
        select: '-password -number',
      })
      .populate('orders')
    success(res, 200, cuadre)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const getCuadresFromToday = async (req, res) => {
  try {
    let orders = await Cuadre.find({
      createdAt: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59)),
      },
    })
      .populate({
        path: 'user',
        select: '-password -number',
      })
      .populate('orders')
    success(res, 200, orders)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const updateCuadre = async (req, res, orderId) => {
  try {
    const cuadre = await Cuadre.findOne({
      user: req.params.id,
      createdAt: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59)),
      },
    })
    cuadre.orders = [...cuadre.orders, orderId]
    await cuadre.save()
  } catch (error) {
    fail(res, 500, error.message)
  }
}

module.exports = {
  createCuadre,
  getCuadres,
  getMyCuadre,
  getCuadresFromToday,
  updateCuadre,
}
