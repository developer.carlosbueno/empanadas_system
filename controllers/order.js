const Order = require('../models/order')
const Cuadre = require('../models/cuadre')
const { success, fail } = require('../utils/response')

const createOrder = async (req, res) => {
  const { totalPrice, orderItems, paymentMethod } = req.body

  try {
    if (orderItems && orderItems.length === 0) {
      fail(res, 400, 'No order items')
    } else {
      const cuadre = await Cuadre.findOne({
        user: req.user._id,
        createdAt: {
          $gte: new Date(new Date().setHours(00, 00, 00)),
          $lt: new Date(new Date().setHours(23, 59, 59)),
        },
      }).populate('orders')
      const createdOrder = await new Order({
        user: req.user._id,
        totalPrice,
        orderItems,
        paymentMethod,
        number: cuadre.orders.length + 1,
      })
      await createdOrder.save()
      cuadre.orders = [createdOrder._id, ...cuadre.orders]
      await cuadre.save()
      success(res, 201, createdOrder)
    }
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const getOrders = async (req, res) => {
  try {
    let orders = await Order.find({}).populate('user')
    success(res, 200, orders)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const getOrdersFromToday = async (req, res) => {
  try {
    let orders = await Order.find({
      createdAt: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59)),
      },
    }).populate('user')
    success(res, 200, orders)
  } catch (error) {
    fail(res, 500, error.message)
  }
}

const getMyOrders = async (req, res) => {
  try {
    let orders = await Order.find({
      user: {
        _id: req.params.id,
      },
      createdAt: {
        $gte: new Date(new Date().setHours(00, 00, 00)),
        $lt: new Date(new Date().setHours(23, 59, 59)),
      },
    }).populate('user')

    success(res, 200, orders)
  } catch (e) {
    fail(res, 500, e.message)
  }
}

module.exports = {
  createOrder,
  getOrders,
  getOrdersFromToday,
  getMyOrders,
}
