const success = (res, status, body) => {
  res.status(status).json({
    error: false,
    body,
  })
}

const fail = (res, status, body) => {
  res.status(status).json({
    error: true,
    body,
  })
}

module.exports = {
  success,
  fail,
}
