const mongoose = require('mongoose')

const { db } = require('./config')

const connectDb = async () => {
  mongoose.connect(db.port, (error) => {
    if (error) throw new Error(error.message)
    console.log('DB IS CONNECTED')
  })
}

module.exports = connectDb()
