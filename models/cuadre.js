const { Schema, model } = require('mongoose')

const cuadreSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  orders: [
    {
      type: Schema.Types.ObjectId,
      ref: 'orders',
    },
  ],
  createdAt: {
    type: Date,
    default: new Date(),
  },
  entryMoney: {
    type: Number,
    required: true,
  },
})

module.exports = model('cuadres', cuadreSchema)
