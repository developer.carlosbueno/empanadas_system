const { Schema, model } = require('mongoose')
const bcrypt = require('bcryptjs')

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  position: {
    type: String,
  },
  number: {
    type: String,
  },
  isAdmin: {
    type: Boolean,
    default: true,
  },
})

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next()
  }
  if (!this.isAdmin) next()
  else this.password = await bcrypt.hash(this.password, 10)
})

userSchema.methods.matchPassword = async function (password) {
  if (!this.isAdmin) {
    return this.password === password
  }
  return await bcrypt.compare(password, this.password)
}

module.exports = model('users', userSchema)
