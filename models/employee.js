const { Schema, model } = require('mongoose')

const employeeSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  totalPrice: {
    type: Number,
    required: true,
  },
  paymentMethod: {
    type: String,
    required: true,
  },
})

module.exports = model('orders', employeeSchema)
