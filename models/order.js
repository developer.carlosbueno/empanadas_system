const { Schema, model } = require('mongoose')

const orderSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  number: {
    type: Number,
    default: 0,
  },
  orderItems: [
    {
      name: { type: String, required: true },
      price: { type: String, required: true },
      qty: { type: Number, required: true },
      product: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'products',
      },
    },
  ],
  totalPrice: {
    type: Number,
    required: true,
  },
  paymentMethod: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
})

orderSchema.pre('save', function (next) {
  console.log(this)
  next()
})

module.exports = model('orders', orderSchema)
