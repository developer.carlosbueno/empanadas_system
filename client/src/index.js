import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Provider } from 'react-redux'
import { SnackbarProvider } from 'notistack'
import store from './store'

// CSS
import './styles/index.css'

const styles = {
  success: { backgroundColor: 'purple' },
  error: { backgroundColor: 'blue' },
  warning: { backgroundColor: 'green' },
  info: { backgroundColor: 'yellow' },
}

ReactDOM.render(
  <Provider store={store}>
    <SnackbarProvider
      maxSnack={3}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      classes={{
        variantSuccess: styles.success,
        variantError: styles.error,
        variantWarning: styles.warning,
        variantInfo: styles.info,
      }}
    >
      <App />
    </SnackbarProvider>
  </Provider>,
  document.getElementById('root')
)
