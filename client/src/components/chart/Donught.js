import { useState } from 'react'
import { Doughnut } from 'react-chartjs-2'

const Donught = () => {
  const [data, setData] = useState({
    labels: ['El chicharron', 'La manosa', 'Cielito lindo'],
    datasets: [
      {
        label: 'Categories',
        data: [25, 17, 11],
        backgroundColor: [
          'rgba(255, 99, 132, 0.6)',
          'rgba(54, 162, 235, 0.6)',
          'rgba(255, 206, 89, 0.6)',
        ],
      },
    ],
  })

  return (
    <div>
      <Doughnut data={data} setData={setData} options={{}} />
    </div>
  )
}

export default Donught
