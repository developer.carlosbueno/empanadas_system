import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'

//Actions
import { logout } from '../actions/users'

//ICONS
import { RiNewspaperFill } from 'react-icons/ri'
import { AiOutlineDashboard } from 'react-icons/ai'
import { MdOutlineInventory2 } from 'react-icons/md'
import { MdOutlineLocalOffer } from 'react-icons/md'
import { BsCashCoin } from 'react-icons/bs'
import { CgMenu } from 'react-icons/cg'
import { FaCashRegister } from 'react-icons/fa'
import { ImUsers } from 'react-icons/im'

//CSS
import '../styles/sidenav.css'
import { BiLogOut } from 'react-icons/bi'

const SideNav = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { userInfo } = useSelector((state) => state.user)
  const [openNav, setOpenNav] = useState(true)

  const handleLogout = () => {
    dispatch(logout())
    navigate('/login')
  }

  return (
    <div className={`sidebar ${openNav ? 'open' : ''}`}>
      <div className='logo-details'>
        <div class='logo_name'>Las Empanadas</div>
        <CgMenu onClick={() => setOpenNav(!openNav)} id='btn' />
      </div>
      <ul className='nav-list'>
        {userInfo?.isAdmin && (
          <>
            <li>
              <Link
                to='/admin/dashboard'
                // className={`${path === 'dashboard' ? 'active' : ''}`}
              >
                <AiOutlineDashboard className='nav_item_icon' />
                <span className='links_name'>Dashboard</span>
              </Link>
              <span className='tooltip'>Dashboard</span>
            </li>
            <li>
              <Link
                to='/admin/inventory'
                // className={`${path === 'inventory' ? 'active' : ''}`}
              >
                <MdOutlineInventory2 className='nav_item_icon' />
                <span className='links_name'>Inventario</span>
              </Link>
              <span className='tooltip'>Inventario</span>
            </li>
            <li>
              <Link
                to='/admin/profit'
                // className={`${path === 'profit' ? 'active' : ''}`}
              >
                <BsCashCoin className='nav_item_icon' />
                <span className='links_name'>Ganancias</span>
              </Link>
              <span className='tooltip'>Ganancias</span>
            </li>
            <li>
              <Link
                to='/admin/facturas'
                // className={`${path === 'profit' ? 'active' : ''}`}
              >
                <FaCashRegister className='nav_item_icon' />
                <span className='links_name'>Cuadre general</span>
              </Link>
              <span className='tooltip'>Cuadre general</span>
            </li>
            <li>
              <Link
                to='/admin/employees'
                // className={`${path === 'dashboard' ? 'active' : ''}`}
              >
                <ImUsers className='nav_item_icon' />
                <span className='links_name'>Empleados</span>
              </Link>
              <span className='tooltip'>Empleados</span>
            </li>
            <li>
              <Link
                to='/admin/promotions'
                // className={`${path === 'dashboard' ? 'active' : ''}`}
              >
                <MdOutlineLocalOffer className='nav_item_icon' />
                <span className='links_name'>Promociones</span>
              </Link>
              <span className='tooltip'>Promociones</span>
            </li>
          </>
        )}
        {!userInfo?.isAdmin && (
          <>
            <li>
              <Link
                to='/facturation'
                // className={`${path === 'orders' ? 'active' : ''}`}
              >
                <RiNewspaperFill className='nav_item_icon' />
                <span className='links_name'>Facturacion</span>
              </Link>
              <span className='tooltip'>Facturacion</span>
            </li>
            <li>
              <Link
                to='/cuadre'
                // className={`${path === 'orders' ? 'active' : ''}`}
              >
                <FaCashRegister className='nav_item_icon' />
                <span className='links_name'>Cuadre</span>
              </Link>
              <span className='tooltip'>Cuadre</span>
            </li>
          </>
        )}
        <li class='profile' onClick={handleLogout}>
          <Link
            to='/admin/inventory'
            // className={`${path === 'dashboard' ? 'active' : ''}`}
          >
            <BiLogOut className='nav_item_icon' />
            <span className='links_name'>Logout</span>
          </Link>
          <span className='tooltip'>Logout</span>
        </li>
      </ul>
    </div>
  )
}

export default SideNav
