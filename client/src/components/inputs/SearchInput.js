import * as React from 'react'
import Paper from '@mui/material/Paper'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '@mui/icons-material/Search'

export default function SearchInput({ value, setValue, placeholder }) {
  return (
    <Paper
      component='form'
      sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400 }}
    >
      <InputBase
        sx={{ ml: 1, flex: 1, height: '40px' }}
        placeholder={`Buscar ${placeholder}`}
        inputProps={{ 'aria-label': 'Buscar producto...' }}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <SearchIcon style={{ marginRight: '10px' }} />
    </Paper>
  )
}
