import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

//Actions
import { deleteProduct } from '../actions/products'

//Components
import { Button } from '@mui/material'
import { BsTrash } from 'react-icons/bs'
import UpdateModal from './modals/inventory/UpdateModal'

const Table = ({ search }) => {
  const dispatch = useDispatch()

  const { products, loading } = useSelector((state) => state.products)

  const [productId, setProductId] = useState('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleDelete = (id) => {
    let answer = window.confirm('Estas seguro de eliminar el producto')
    if (answer) {
      dispatch(deleteProduct(id))
      window.location.reload()
    }
  }

  const handleUpdateModal = (id) => {
    handleOpen()
    setProductId(id)
  }

  return (
    <>
      {loading && 'Loading'}
      {products && (
        <table className='table'>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Categoria</th>
              <th>Precio</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {products
              ?.filter((val) => {
                if (search === '') return val
                else if (val.name.toLowerCase().includes(search.toLowerCase()))
                  return val
              })
              .map((product) => (
                <tr
                  className='table__row'
                  onClick={() => handleUpdateModal(product._id)}
                >
                  <td>{product.name}</td>
                  <td>{product.category}</td>
                  <td>${product.price}</td>
                  <td style={{ width: '20px' }}>
                    <Button
                      variant='contained'
                      color='error'
                      onClick={() => handleDelete(product._id)}
                    >
                      <BsTrash size='20px' />
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      )}
      <UpdateModal open={open} handleClose={handleClose} id={productId} />
    </>
  )
}

export default Table
