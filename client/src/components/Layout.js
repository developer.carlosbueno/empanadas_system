import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'

//Components
import SideNav from './SideNav'

const Layout = ({ children }) => {
  const location = useLocation()
  const navigate = useNavigate()

  const { userInfo } = useSelector((state) => state.user)
  const path = location.pathname.split('/')[1]

  useEffect(() => {
    if (path !== 'login' && !userInfo) {
      navigate('/login')
    }
    if (path === 'admin' && userInfo && !userInfo?.isAdmin) {
      navigate('/facturation')
    }
  }, [path, navigate, userInfo])

  return (
    <>
      {path !== 'login' && path !== 'print' && path !== 'print_cuadre' && (
        <SideNav />
      )}
      {children}
    </>
  )
}

export default Layout
