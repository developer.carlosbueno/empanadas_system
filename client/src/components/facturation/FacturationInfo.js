import { useState } from 'react'
import { useSelector } from 'react-redux'

//Components
import {
  Button,
  TextField,
  IconButton,
  MenuItem,
  FormControl,
  InputLabel,
  Select,
} from '@mui/material'

//Icons
import { BsTrash } from 'react-icons/bs'

function FacturationInfo({
  subtotal,
  itbis,
  total,
  tip,
  itemsQty,
  paymentMethod,
  setPaymentMethod,
  handleCreateOrder,
  seletedProducts,
  handleDelete,
  setOffice,
  cash,
  setCash,
}) {
  const { userInfo } = useSelector((state) => state.user)

  return (
    <form className='facturationInfo'>
      <div className='facturationInfo__header'>
        {userInfo.position !== 'Hugo & Delivery' && (
          <>
            <div className='facturationInfo__subtotal'>
              <p>Subtotal</p>
              <p>RD ${subtotal.toFixed(2)}</p>
            </div>
            <div className='facturationInfo__itbis'>
              <p>18% ITBIS</p>
              <p>RD ${itbis.toFixed(2)}</p>
            </div>
            <div className='facturationInfo__itbis'>
              <p>10% Propina</p>
              <p>RD ${tip.toFixed(2)}</p>
            </div>
            <div className='facturationInfo__paymentMethod'>
              <h4>Agregar Pago</h4>
              <div className='facturationInfo__paymentMethod--options'>
                <div
                  className={`facturationInfo__paymentMethod--options-items ${
                    paymentMethod === 'Efectivo' ? 'selected' : ''
                  }`}
                  onClick={() => setPaymentMethod('Efectivo')}
                >
                  Efectivo
                </div>
                <div
                  className={`facturationInfo__paymentMethod--options-items ${
                    paymentMethod === 'Tarjeta' ? 'selected' : ''
                  }`}
                  onClick={() => setPaymentMethod('Tarjeta')}
                >
                  Tarjeta
                </div>
                <div
                  className={`facturationInfo__paymentMethod--options-items ${
                    paymentMethod === 'Oficina' ? 'selected' : ''
                  }`}
                  onClick={() => setPaymentMethod('Oficina')}
                  style={{ marginTop: '10px' }}
                >
                  Oficina
                </div>
              </div>
            </div>
            {paymentMethod === 'Efectivo' && (
              <div className='facturationInfo__paymentCash'>
                <TextField
                  style={{ width: '100%' }}
                  value={cash}
                  onChange={(e) => setCash(e.target.value)}
                  type='number'
                  placeholder='recibido'
                />
                <p>Cambio: ${Math.round(cash - total)}</p>
              </div>
            )}
            {paymentMethod === 'Oficina' && (
              <div className='facturationInfo__paymentCash'>
                <FormControl
                  variant='standard'
                  sx={{ m: 1, minWidth: 120, width: '100%' }}
                >
                  <InputLabel id='demo-simple-select-standard-label'>
                    Persona
                  </InputLabel>
                  <Select
                    labelId='demo-simple-select-standard-label'
                    id='demo-simple-select-standard'
                    label='Office'
                    onChange={(e) => setOffice(e.target.value)}
                  >
                    <MenuItem value='Jorge'>Jorge</MenuItem>
                    <MenuItem value='Victor'>Victor</MenuItem>
                    <MenuItem value='Tete'>Tete</MenuItem>
                    <MenuItem value='Alfredo'>Alfredo</MenuItem>
                  </Select>
                </FormControl>
              </div>
            )}
          </>
        )}
        <div className='facturationInfo__products'>
          <h4>Productos</h4>
          <ul>
            {seletedProducts?.map((p) => (
              <li>
                <p>{p.name}</p>
                <p>{p.qty}</p>
                <IconButton onClick={() => handleDelete(p.name)}>
                  <BsTrash size='20px' color='#ED6E6E' />
                </IconButton>
              </li>
            ))}
          </ul>
        </div>
      </div>

      <div className='facturationInfo__body'>
        <div className='facturationInfo__body--info'>
          <div className='facturationInfo__body--info-item'>
            <p>Articulos</p>
            <h3>{itemsQty}</h3>
          </div>
          <div className='facturationInfo__body--info-item'>
            <p>Total</p>
            <h3>RD ${total.toFixed(2)}</h3>
          </div>
        </div>

        <Button
          variant='contained'
          color='success'
          type='submit'
          style={{ width: '100%', height: '50px' }}
          onClick={handleCreateOrder}
        >
          Facturar
        </Button>
      </div>
    </form>
  )
}

export default FacturationInfo
