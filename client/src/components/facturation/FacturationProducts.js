//Components
import { Divider } from '@mui/material'

function FacturationProducts({ products, handleAddProduct, selectedCategory }) {
  let productsByCategory = products.filter(
    (p) => p.category === selectedCategory
  )

  const addClassColor = (price) => {
    if (price <= 97.66) {
      return 'green'
    }
    if (price === 117.19) {
      return 'yellow'
    }
    if (price >= 136.72) {
      return 'red'
    }
  }

  return (
    <>
      <div className='products-section'>
        <Divider component='div' />
        <div className='products__container'>
          {productsByCategory.map((p) => {
            let color = addClassColor(p.price)
            return (
              <div
                key={p.name}
                className={`products__container--items ${color}`}
                onClick={() => handleAddProduct(p.name, p.price, 1, p._id)}
              >
                {p.name}
              </div>
            )
          })}
        </div>
      </div>
    </>
  )
}

export default FacturationProducts
