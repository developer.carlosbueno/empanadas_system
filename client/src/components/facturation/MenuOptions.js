import React from 'react'

function MenuOptions({ selectedCategory, setSelectedCategory }) {
  return (
    <div className='container menu-options'>
      <div
        className={`item menu-buttom ${
          selectedCategory === 'empanadas' ? 'selected' : ''
        }`}
        onClick={() => setSelectedCategory('empanadas')}
      >
        Empanadas
      </div>
      <div
        className={`item menu-buttom ${
          selectedCategory === 'bebidas' ? 'selected' : ''
        }`}
        onClick={() => setSelectedCategory('bebidas')}
      >
        Bebidas
      </div>
      <div
        className={`item menu-buttom ${
          selectedCategory === 'dulces' ? 'selected' : ''
        }`}
        onClick={() => setSelectedCategory('dulces')}
      >
        Dulces
      </div>
    </div>
  )
}

export default MenuOptions
