import { useState } from 'react'
import { useDispatch } from 'react-redux'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { FormControl, TextField, Divider } from '@mui/material'
import Modal from '@mui/material/Modal'
import FormatNumber from '../../inputs/FormatNumber'

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

export default function CreateModal({ open, handleClose, handlePrint }) {
  const [cash, setCash] = useState('')
  const [card, setCard] = useState('')
  const [notes, setNotes] = useState('')

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <h2>Terminar cuadre</h2>
          <Divider component='h2' />
          <FormControl
            variant='standard'
            sx={{ m: 1, minWidth: 120, width: '60%' }}
          >
            <FormatNumber
              values={cash}
              setValues={setCash}
              label='Efectivo cuadre'
            />
          </FormControl>
          <FormControl
            variant='standard'
            sx={{ m: 1, minWidth: 120, width: '60%' }}
          >
            <FormatNumber
              values={card}
              setValues={setCard}
              label='Tarjeta cuadre'
            />
          </FormControl>
          <Divider />
          <FormControl>
            <p style={{ marginBottom: '10px', marginTop: '10px' }}>Notas</p>
            <textarea
              value={notes}
              onChange={(e) => setNotes(e.target.value)}
              style={{
                width: '400px',
                height: '100px',
                marginBottom: '20px',
                fontSize: '18px',
                padding: '10px',
              }}
            />
          </FormControl>
          <Button
            variant='contained'
            type='submit'
            style={{
              width: '400px',
            }}
            onClick={() => handlePrint(cash, card, notes)}
          >
            Imprimir
          </Button>
        </Box>
      </Modal>
    </div>
  )
}
