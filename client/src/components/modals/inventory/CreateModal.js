import { useState } from 'react'
import { useDispatch } from 'react-redux'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material'
import Modal from '@mui/material/Modal'
import { Divider } from '@mui/material'
import FormatNumber from '../../inputs/FormatNumber'

// Actions
import { createProduct } from '../../../actions/products'

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

export default function CreateModal({ open, handleClose }) {
  const dispatch = useDispatch()

  const [category, setCategory] = useState('')
  const [name, setName] = useState('')
  const [price, setPrice] = useState('')

  const handleCreate = () => {
    dispatch(
      createProduct({
        name,
        price,
        category,
      })
    )
  }

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <h2>Agregar producto</h2>
          <Divider component='h2' />

          <form className='grid_columns_2' onSubmit={handleCreate}>
            <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
              <TextField
                label='Nombre'
                variant='standard'
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormControl>

            <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
              <FormatNumber
                label={'Precio'}
                values={price}
                setValues={setPrice}
              />
            </FormControl>

            <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
              <InputLabel id='demo-simple-select-standard-label'>
                Categoria
              </InputLabel>
              <Select
                labelId='demo-simple-select-standard-label'
                id='demo-simple-select-standard'
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                label='Categoria'
              >
                <MenuItem value='empanadas'>Empanadas</MenuItem>
                <MenuItem value='bebidas'>Bebidas</MenuItem>
                <MenuItem value='dulces'>Dulces</MenuItem>
              </Select>
            </FormControl>

            <Button
              variant='contained'
              type='submit'
              style={{
                height: '80%',
                alignSelf: 'end',
              }}
            >
              Aceptar
            </Button>
          </form>
        </Box>
      </Modal>
    </div>
  )
}
