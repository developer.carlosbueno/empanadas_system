import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material'
import Modal from '@mui/material/Modal'
import { Divider } from '@mui/material'
import FormatNumber from '../../inputs/FormatNumber'

// Actions
import { updateProduct } from '../../../actions/products'

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

export default function CreateModal({ open, handleClose, id }) {
  const dispatch = useDispatch()

  const { products } = useSelector((state) => state.products)

  let product = products?.filter((p) => p._id === id)[0]

  const [promotion, setPromotion] = useState('')
  const [name, setName] = useState(product?.name || '')
  const [price, setPrice] = useState(product?.price || '')

  const handleUpdate = () => {
    dispatch(
      updateProduct(product._id, {
        name,
        price,
      })
    )
  }

  const cleanState = () => {
    setPromotion('')
    setName('')
    setPrice('')
  }

  useEffect(() => {
    const fillState = () => {
      if (product) {
        setPromotion('')
        setName(product.name)
        setPrice(product.price)
      }
    }
    cleanState()
    fillState()
  }, [id, product])

  return (
    <>
      {product && (
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby='modal-modal-title'
          aria-describedby='modal-modal-description'
        >
          <Box sx={style}>
            <h2>Actualizar producto</h2>
            <Divider component='h2' />

            <form className='grid_columns_2' onSubmit={handleUpdate}>
              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <TextField
                  label='Nombre'
                  variant='standard'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </FormControl>

              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <FormatNumber
                  label={'Precio'}
                  values={price}
                  setValues={setPrice}
                />
              </FormControl>

              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id='demo-simple-select-standard-label'>
                  Promocion
                </InputLabel>
                <Select
                  labelId='demo-simple-select-standard-label'
                  id='demo-simple-select-standard'
                  value={promotion}
                  onChange={(e) => setPromotion(e.target.value)}
                  label='Age'
                >
                  <MenuItem value=''>
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <Button
                variant='contained'
                type='submit'
                style={{
                  height: '80%',
                  alignSelf: 'end',
                }}
              >
                Aceptar
              </Button>
            </form>
          </Box>
        </Modal>
      )}
    </>
  )
}
