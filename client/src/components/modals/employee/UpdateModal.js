import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Divider,
  FormControlLabel,
  Checkbox,
} from '@mui/material'
import Modal from '@mui/material/Modal'

// Actions
import { updateUser } from '../../../actions/users'

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

export default function UpdateModal({ open, handleClose, id }) {
  const dispatch = useDispatch()

  const { users } = useSelector((state) => state.userList)
  let user = users?.filter((p) => p._id === id)[0]

  const [name, setName] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [position, setPosition] = useState('')
  const [number, setNumber] = useState('')
  const [isAdmin, setIsAdmin] = useState(false)

  const cleanState = () => {
    setPosition('')
    setName('')
    setNumber('')
    setIsAdmin('')
  }

  useEffect(() => {
    const fillState = () => {
      if (user) {
        setPosition(user.position)
        setName(user.name)
        setUsername(user.username)
        setPassword(user.password)
        setNumber(user.number)
        setIsAdmin(user.isAdmin)
      }
    }
    cleanState()
    fillState()
  }, [id, user])

  const handleUpdate = () => {
    dispatch(
      updateUser(user._id, {
        name,
        position,
        number,
        isAdmin,
        username,
        password,
      })
    )
    window.location.reload()
  }

  return (
    <>
      {user && (
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby='modal-modal-title'
          aria-describedby='modal-modal-description'
        >
          <Box sx={style}>
            <h2>Actualizar empleado</h2>
            <Divider component='h2' />

            <form>
              <div className='grid_columns_2'>
                <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                  <TextField
                    label='Nombre'
                    variant='standard'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </FormControl>

                <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                  <InputLabel id='demo-simple-select-standard-label'>
                    Posición
                  </InputLabel>
                  <Select
                    labelId='demo-simple-select-standard-label'
                    id='demo-simple-select-standard'
                    value={position}
                    onChange={(e) => setPosition(e.target.value)}
                    label='Categoria'
                  >
                    <MenuItem value='Head Chef'>Head Chef</MenuItem>
                    <MenuItem value='Chef'>Chef</MenuItem>
                    <MenuItem value='Manager'>Manager</MenuItem>
                    <MenuItem value='Hugo & Delivery'>Hugo & Delivery</MenuItem>
                    <MenuItem value='Steward'>Steward</MenuItem>
                    <MenuItem value='Cajera'>Cajera</MenuItem>
                  </Select>
                </FormControl>

                <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                  <TextField
                    label='Numero de telefono'
                    variant='standard'
                    type='number'
                    value={number}
                    onChange={(e) => setNumber(e.target.value)}
                  />
                </FormControl>

                <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                  <FormControlLabel
                    control={<Checkbox checked={isAdmin} />}
                    label='Administrador'
                    value={isAdmin}
                    onChange={() => setIsAdmin(!isAdmin)}
                  />
                </FormControl>
              </div>

              <Divider component='div' />
              <h4 style={{ marginTop: '10px' }}>Informacion de cuenta</h4>
              <div className='grid_columns_2'>
                <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                  <TextField
                    label='Username'
                    variant='standard'
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </FormControl>
                <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                  <TextField
                    label='Password'
                    variant='standard'
                    type='password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </FormControl>
              </div>

              <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <Button
                  variant='contained'
                  color='error'
                  style={{
                    height: '80%',
                    alignSelf: 'end',
                    marginTop: '20px',
                    width: '45%',
                  }}
                >
                  Eliminar
                </Button>
                <Button
                  variant='contained'
                  style={{
                    height: '80%',
                    alignSelf: 'end',
                    marginTop: '20px',
                    width: '45%',
                  }}
                  onClick={handleUpdate}
                >
                  Aceptar
                </Button>
              </div>
            </form>
          </Box>
        </Modal>
      )}
    </>
  )
}
