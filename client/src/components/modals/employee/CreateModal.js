import { useState } from 'react'
import { useDispatch } from 'react-redux'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Divider,
  FormControlLabel,
  Checkbox,
} from '@mui/material'
import Modal from '@mui/material/Modal'

// Actions
import { registerUser } from '../../../actions/users'

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

export default function CreateModal({ open, handleClose }) {
  const dispatch = useDispatch()

  const [name, setName] = useState('')
  const [position, setPosition] = useState('')
  const [number, setNumber] = useState('')
  const [isAdmin, setIsAdmin] = useState(false)

  const handleCreate = (e) => {
    dispatch(
      registerUser({
        name,
        position,
        number,
        isAdmin,
      })
    )
  }

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <h2>Agregar empleado</h2>
          <Divider component='h2' />

          <form onSubmit={handleCreate} style={{ textAlign: 'center' }}>
            <div className='grid_columns_2'>
              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <TextField
                  label='Nombre'
                  variant='standard'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </FormControl>

              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id='demo-simple-select-standard-label'>
                  Posición
                </InputLabel>
                <Select
                  labelId='demo-simple-select-standard-label'
                  id='demo-simple-select-standard'
                  value={position}
                  onChange={(e) => setPosition(e.target.value)}
                  label='Categoria'
                >
                  <MenuItem value='Head Chef'>Head Chef</MenuItem>
                  <MenuItem value='Chef'>Chef</MenuItem>
                  <MenuItem value='Manager'>Manager</MenuItem>
                  <MenuItem value='Hugo & Delivery'>Hugo & Delivery</MenuItem>
                  <MenuItem value='Steward'>Steward</MenuItem>
                  <MenuItem value='Cajera'>Cajera</MenuItem>
                </Select>
              </FormControl>

              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <TextField
                  label='Numero de telefono'
                  variant='standard'
                  type='number'
                  value={number}
                  onChange={(e) => setNumber(e.target.value)}
                />
              </FormControl>

              <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
                <FormControlLabel
                  control={<Checkbox />}
                  label='Administrador'
                  value={isAdmin}
                  onChange={() => setIsAdmin(!isAdmin)}
                />
              </FormControl>
            </div>

            <Button
              variant='contained'
              type='submit'
              style={{
                height: '80%',
                alignSelf: 'end',
                marginTop: '20px',
                width: '60%',
              }}
            >
              Aceptar
            </Button>
          </form>
        </Box>
      </Modal>
    </div>
  )
}
