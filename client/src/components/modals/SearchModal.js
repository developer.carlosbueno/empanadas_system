import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

//Components
import SearchInput from '../inputs/SearchInput'
import Box from '@mui/material/Box'
import Modal from '@mui/material/Modal'
import { Divider } from '@mui/material'

const style = {
  position: 'absolute',
  top: '40%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  p: 4,
}

export default function SearchModal({
  open,
  handleClose,
  products,
  seletedProducts,
  handleAddProduct,
}) {
  const [search, setSearch] = useState('')

  let items = products?.filter((p) =>
    p.name.toLowerCase().includes(search.toLocaleLowerCase())
  )

  let showingProducts = items.map((i) => {
    let pro = seletedProducts.filter((p) => p.name === i.name)
    console.log(pro)
    console.log(i, ';')
    if (pro) i.qty = pro.qty
    return i
  })

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <h2 style={{ textAlign: 'center', marginBottom: '10px' }}>
            Buscar producto
          </h2>
          <Divider component='h2' />

          <SearchInput value={search} setValue={setSearch} />

          <div className='search__modal--tableContainer'>
            <table className='table'>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Precio</th>
                </tr>
              </thead>
              <tbody>
                {showingProducts?.map((product) => (
                  <tr
                    className='table__row'
                    onClick={() =>
                      handleAddProduct(product.name, product.price, 1)
                    }
                  >
                    <td>{product.name}</td>
                    <td className='relative'>
                      ${product.price}
                      <div className='badge'>{product.qty}</div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </Box>
      </Modal>
    </div>
  )
}
