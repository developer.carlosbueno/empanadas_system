import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

//Actions
import { deleteProduct } from '../../actions/products'

//Components
import UpdateModal from '../modals/employee/UpdateModal'

const Table = ({ search }) => {
  const { users } = useSelector((state) => state.userList)
  const dispatch = useDispatch()

  const [productId, setProductId] = useState('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleDelete = (id) => {
    let answer = window.confirm('Estas seguro de eliminar el producto')
    if (answer) {
      dispatch(deleteProduct(id))
      window.location.reload()
    }
  }

  const handleUpdateModal = (id) => {
    handleOpen()
    setProductId(id)
  }

  return (
    <>
      <table className='table'>
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Username</th>
            <th>Posición</th>
            <th>Celular</th>
          </tr>
        </thead>
        <tbody>
          {users?.map((user) => (
            <tr
              className='table__row'
              onClick={() => handleUpdateModal(user._id)}
            >
              <td>{user.name}</td>
              <td>{user.username}</td>
              <td>{user.position}</td>
              <td>{user.number}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <UpdateModal open={open} handleClose={handleClose} id={productId} />
    </>
  )
}

export default Table
