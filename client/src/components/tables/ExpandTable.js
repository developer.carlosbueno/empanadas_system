import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import IconButton from '@mui/material/IconButton'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Typography from '@mui/material/Typography'
import Paper from '@mui/material/Paper'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import { Button } from '@material-ui/core'

//Utils
import formatDate from '../../utils/formatDate'

//Actions
import { getPrintData } from '../../actions/print'
import { listMyOrders } from '../../actions/orders'

function createData(
  number,
  items,
  paymentMethod,
  totalPrice,
  history,
  createdAt
) {
  return {
    number,
    items,
    paymentMethod,
    totalPrice,
    history,
    createdAt,
  }
}

function Row(props) {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { row } = props
  const [open, setOpen] = React.useState(false)
  const { userInfo } = useSelector((state) => state.user)

  const handlePrint = () => {
    dispatch(listMyOrders())
    dispatch(
      getPrintData({
        name: userInfo?.name || 'Las Empanadas',
        orderItems: row.history,
        time: new Date(row.createdAt),
        number: row.number,
      })
    )

    navigate('/print')
  }

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label='expand row'
            size='small'
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component='th' scope='row'>
          {row.number}
        </TableCell>
        <TableCell component='th' scope='row' align='right'>
          {row.items}
        </TableCell>
        <TableCell align='right'>{row.paymentMethod}</TableCell>
        <TableCell align='right'>${row.totalPrice}</TableCell>
        <TableCell align='right'>
          {formatDate(new Date(row.createdAt), 'time')}
        </TableCell>
        <TableCell align='right'>
          <Button variant='contained' onClick={handlePrint}>
            ok
          </Button>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant='h6' gutterBottom component='div'>
                Articulos
              </Typography>
              <Table size='small' aria-label='purchases'>
                <TableHead>
                  <TableRow>
                    <TableCell>Nombre</TableCell>
                    <TableCell align='right'>Cantidad</TableCell>
                    <TableCell align='right'>Precio total ($)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.history.map((historyRow) => (
                    <TableRow key={historyRow.name}>
                      <TableCell component='th' scope='row'>
                        {historyRow.name}
                      </TableCell>
                      <TableCell align='right'>{historyRow.qty}</TableCell>
                      <TableCell align='right'>
                        {Math.round(historyRow.qty * historyRow.price * 100) /
                          100}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

export default function CollapsibleTable({ info }) {
  const rows = []

  info?.forEach((i) => {
    rows.push(
      createData(
        i.number,
        i.orderItems.reduce((acc, current) => acc + current.qty, 0),
        i.paymentMethod,
        i.totalPrice,
        i.orderItems,
        i.createdAt
      )
    )
  })

  return (
    <TableContainer component={Paper}>
      <Table aria-label='collapsible table'>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Numero</TableCell>
            <TableCell align='right'>Articulos</TableCell>
            <TableCell align='right'>Metodo de pago</TableCell>
            <TableCell align='right'>Monto</TableCell>
            <TableCell align='right'>Hora</TableCell>
            <TableCell align='right'>Imprimir</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row key={row.name} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
