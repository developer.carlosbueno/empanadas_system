import axios from 'axios'

export let actions = {
  PRODUCT_LIST_REQUEST: 'PRODUCT_LIST_REQUEST',
  PRODUCT_LIST_SUCCESS: 'PRODUCT_LIST_SUCCESS',
  PRODUCT_LIST_FAIL: 'PRODUCT_LIST_FAIL',
  PRODUCT_DETAIL_REQUEST: 'PRODUCT_DETAIL_REQUEST',
  PRODUCT_DETAIL_SUCCESS: 'PRODUCT_DETAIL_SUCCESS',
  PRODUCT_DETAIL_FAIL: 'PRODUCT_DETAIL_FAIL',
  PRODUCT_DETAIL_RESET: 'PRODUCT_DETAIL_RESET',
  PRODUCT_CREATE_REQUEST: 'PRODUCT_CREATE_REQUEST',
  PRODUCT_CREATE_SUCCESS: 'PRODUCT_CREATE_SUCCESS',
  PRODUCT_CREATE_FAIL: 'PRODUCT_CREATE_FAIL',
  PRODUCT_UPDATE_REQUEST: 'PRODUCT_UPDATE_REQUEST',
  PRODUCT_UPDATE_SUCCESS: 'PRODUCT_UPDATE_SUCCESS',
  PRODUCT_UPDATE_FAIL: 'PRODUCT_UPDATE_FAIL',
}

export const listProducts = () => async (dispatch) => {
  try {
    dispatch({ type: actions.PRODUCT_LIST_REQUEST })

    const { data } = await axios.get('/api/products')

    dispatch({
      type: actions.PRODUCT_LIST_SUCCESS,
      payload: data.body,
    })
  } catch (error) {
    console.log(error)
    dispatch({
      type: actions.PRODUCT_LIST_FAIL,
      payload: error.message,
    })
  }
}

export const detailProducts = (id) => async (dispatch) => {
  try {
    dispatch({ type: actions.PRODUCT_DETAIL_REQUEST })

    const { data } = await axios.get(`/api/products/${id}`)

    dispatch({
      type: actions.PRODUCT_DETAIL_SUCCESS,
      payload: data.body,
    })
  } catch (error) {
    dispatch({
      type: actions.PRODUCT_DETAIL_FAIL,
      payload: 'Error fetching the data',
    })
  }
}

export const createProduct = (product) => async (dispatch, getState) => {
  try {
    dispatch({ type: actions.PRODUCT_CREATE_REQUEST })

    const { userInfo } = getState().user

    let config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.post(`/api/products`, product, config)

    dispatch({
      type: actions.PRODUCT_CREATE_SUCCESS,
      payload: data.body,
    })
  } catch (error) {
    dispatch({
      type: actions.PRODUCT_CREATE_FAIL,
      payload: error.message,
    })
  }
}

export const updateProduct = (id, product) => async (dispatch, getState) => {
  try {
    dispatch({ type: actions.PRODUCT_UPDATE_REQUEST })

    const { userInfo } = getState().user

    let config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.put(`/api/products/${id}`, product, config)

    dispatch({
      type: actions.PRODUCT_UPDATE_SUCCESS,
      payload: data.body,
    })
  } catch (error) {
    dispatch({
      type: actions.PRODUCT_UPDATE_FAIL,
      payload: error.message,
    })
  }
}

export const deleteProduct = (id) => async (dispatch, getState) => {
  try {
    const { userInfo } = getState().user

    let config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    await axios.delete(`/api/products/${id}`, config)
  } catch (error) {
    console.log(error)
  }
}

export const productDetailReset = () => (dispatch) => {
  dispatch({ type: actions.PRODUCT_DETAIL_RESET })
}
