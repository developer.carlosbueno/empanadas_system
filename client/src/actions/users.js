import axios from 'axios'

export const actions = {
  USER_LOGIN_REQUEST: 'USER_LOGIN_REQUEST',
  USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',
  USER_LOGIN_FAIL: 'USER_LOGIN_FAIL',
  USER_REGISTER_REQUEST: 'USER_REGISTER_REQUEST',
  USER_REGISTER_SUCCESS: 'USER_REGISTER_SUCCESS',
  USER_REGISTER_FAIL: 'USER_REGISTER_FAIL',
  USER_DETAILS_REQUEST: 'USER_DETAILS_REQUEST',
  USER_DETAILS_SUCCESS: 'USER_DETAILS_SUCCESS',
  USER_DETAILS_FAIL: 'USER_DETAILS_FAIL',
  USER_LIST_REQUEST: 'USER_LIST_REQUEST',
  USER_LIST_SUCCESS: 'USER_LIST_SUCCESS',
  USER_LIST_FAIL: 'USER_LIST_FAIL',
  USER_LIST_RESET: 'USER_LIST_RESET',
  USER_UPDATE_REQUEST: 'USER_UPDATE_REQUEST',
  USER_UPDATE_SUCCESS: 'USER_UPDATE_SUCCESS',
  USER_UPDATE_FAIL: 'USER_UPDATE_FAIL',
  USER_LOGOUT: 'USER_LOGOUT',
}

export const loginUser = (username, password) => async (dispatch) => {
  try {
    dispatch({ type: actions.USER_LOGIN_REQUEST })

    let config = {
      'Content-Type': 'application/json',
    }

    const { data } = await axios.post(
      '/api/users/login',
      {
        username,
        password,
      },
      config
    )
    dispatch({ type: actions.USER_LOGIN_SUCCESS, payload: data.body })

    localStorage.setItem('userInfo', JSON.stringify(data.body))
  } catch (error) {
    dispatch({
      type: actions.USER_LOGIN_FAIL,
      payload: error.response.data.body,
    })
  }
}

export const registerUser = (info) => async (dispatch, getState) => {
  try {
    dispatch({ type: actions.USER_REGISTER_REQUEST })

    const { userInfo } = getState().user

    let config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    let randomNumber = Math.floor(Math.random() * 10000)
    let username = info.name.replace(' ', '')
    let password = username + randomNumber

    const { data } = await axios.post(
      '/api/users/register',
      {
        ...info,
        username: username.toLowerCase(),
        password,
      },
      config
    )

    dispatch({ type: actions.USER_REGISTER_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({
      type: actions.USER_REGISTER_FAIL,
      payload: error.response.data.body,
    })
  }
}

export const usersDetails = () => async (dispatch, getState) => {
  try {
    dispatch({ type: actions.USER_DETAILS_REQUEST })

    const { userInfo } = getState().user
    let config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.get(
      `/api/users/profile/${userInfo._id}`,
      config
    )

    dispatch({ type: actions.USER_DETAILS_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.USER_DETAILS_FAIL, payload: error.message })
  }
}

export const listUsers = () => async (dispatch, getState) => {
  try {
    dispatch({ type: actions.USER_LIST_REQUEST })

    const { userInfo } = getState().user
    let config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.get('/api/users', config)

    dispatch({ type: actions.USER_LIST_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.USER_LIST_FAIL, payload: error.message })
  }
}

export const updateUser = (id, info) => async (dispatch, getState) => {
  dispatch({ type: actions.USER_UPDATE_REQUEST })

  try {
    const { userInfo } = getState().user
    let config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.put(`/api/users/${id}`, info, config)
    dispatch({ type: actions.USER_LIST_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.USER_LIST_FAIL, payload: error.message })
  }
}

export const logout = () => (dispatch) => {
  dispatch({ type: actions.USER_LOGOUT })
  localStorage.removeItem('userInfo')
}
