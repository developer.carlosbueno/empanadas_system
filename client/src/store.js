import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

//Reducers
import {
  listProductReducer,
  detailProductReducer,
  createProductReducer,
  updateProductReducer,
} from './reducers/products'
import {
  userLoginReducer,
  userRegisterReducer,
  userListReducer,
  userDetailsReducer,
  userUpdateReducer,
} from './reducers/users'
import { printReducer } from './reducers/print'
import {
  orderListReducer,
  orderMyListReducer,
  orderReducer,
} from './reducers/orders'
import { cuadreReducer, cuadresReducer } from './reducers/cuadres'

const localUser = localStorage.getItem('userInfo')
  ? JSON.parse(localStorage.getItem('userInfo'))
  : null

const reducers = combineReducers({
  products: listProductReducer,
  product: detailProductReducer,
  createdProduct: createProductReducer,
  updatedProduct: updateProductReducer,
  user: userLoginReducer,
  userRegister: userRegisterReducer,
  userDetails: userDetailsReducer,
  userList: userListReducer,
  userUpdate: userUpdateReducer,
  print: printReducer,
  orders: orderListReducer,
  createdOrder: orderReducer,
  myOrders: orderMyListReducer,
  cuadre: cuadreReducer,
  cuadres: cuadresReducer,
})

let initialState = {
  user: {
    userInfo: localUser,
  },
  products: [],
}

let store = createStore(
  reducers,
  initialState,
  composeWithDevTools(applyMiddleware(thunk))
)

export default store
