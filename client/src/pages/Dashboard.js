import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

//Actions
import { listTodayOrders } from '../actions/orders'
import { listUsers } from '../actions/users'

//Icons
import { FiUsers } from 'react-icons/fi'
import { FiShoppingBag } from 'react-icons/fi'
import { MdOutlineOutbox } from 'react-icons/md'
import { BiTaskX } from 'react-icons/bi'

//CSS
import '../styles/dashboard.css'

//Components
import Donught from '../components/chart/Donught'
import LineChart from '../components/chart/Line'

const Dashboard = () => {
  const dispatch = useDispatch()

  const { orders } = useSelector((state) => state.orders)
  const { users } = useSelector((state) => state.userList)
  console.log(orders)
  console.log(orders?.reduce((acc, c) => acc + c.totalPrice, 0))

  useEffect(() => {
    dispatch(listTodayOrders())
    dispatch(listUsers())
  }, [dispatch])

  return (
    <div className='panel'>
      <h1 className='panel__section'>Dashboard</h1>
      <div className='panel__container'>
        <div className='panelCards'>
          <div className='panel__general'>
            <div className='general__card margin_none'>
              <div className='general__card-info'>
                <h2>{users?.filter((u) => !u.isAdmin).length}</h2>
                <p>Empleados</p>
              </div>
              <FiUsers />
            </div>
            <div className='general__card'>
              <div className='general__card-info'>
                <h2>{orders?.length}</h2>
                <p>Facturas</p>
              </div>
              <FiShoppingBag />
            </div>
            <div className='general__card'>
              <div className='general__card-info'>
                <h2>3</h2>
                <p>Tareas</p>
              </div>
              <BiTaskX />
            </div>
            <div className='general__card'>
              <div className='general__card-info'>
                <h2>{orders?.reduce((acc, c) => acc + c.totalPrice, 0)}</h2>
                <p>Ganancias</p>
              </div>
              <MdOutlineOutbox />
            </div>
          </div>

          <div className='panel__chart'>
            <div className='panel__chart-section'>
              <LineChart />
            </div>
            <div className='panel__chart-section'>
              <Donught />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Dashboard
