import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Navigate, useNavigate } from 'react-router'

//Actions
import { createCuadre, getMyCuadre } from '../actions/cuadre'

//Components
import { FiUsers } from 'react-icons/fi'
import { RiNewspaperFill } from 'react-icons/ri'
import ExpandTable from '../components/tables/ExpandTable'
import FormatNumber from '../components/inputs/FormatNumber'
import { Button } from '@mui/material'
import CreateModal from '../components/modals/cuadre/create'

//Css
import '../styles/cuadre.css'
import { getPrintData } from '../actions/print'
import Loading from '../components/Loading'

const Cuadre = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { cuadre, loading } = useSelector((state) => state.cuadre)
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const [entryMoney, setEntryMoney] = useState(
    cuadre ? Number(cuadre.entryMoney) : ''
  )

  let items = cuadre && cuadre?.orders
  let cash = items
    ?.filter((i) => i.paymentMethod === 'Efectivo')
    .reduce((acc, i) => acc + i.totalPrice, 0)
  let card = items
    ?.filter((i) => i.paymentMethod === 'Tarjeta')
    .reduce((acc, i) => acc + i.totalPrice, 0)
  let office = items
    ?.filter((i) => i.paymentMethod === 'Oficina')
    .reduce((acc, i) => acc + i.totalPrice, 0)

  useEffect(() => {
    dispatch(getMyCuadre())
  }, [dispatch])

  const handleCreateCuadre = () => {
    dispatch(createCuadre(entryMoney))
  }

  const handlePrintCuadre = (cash, card, notes) => {
    dispatch(getPrintData(items, true, cash, card, notes))
    navigate('/print_cuadre')
  }

  return (
    <div className='panel'>
      <h1
        className='panel__section'
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        Cuadre personal
        <div>
          <Button
            variant='contained'
            color='success'
            style={{ marginRight: '10px' }}
            onClick={handleOpen}
          >
            Cuadrar
          </Button>
        </div>
      </h1>
      {loading ? (
        <Loading />
      ) : (
        <div className='cuadre'>
          {cuadre && <ExpandTable info={cuadre?.orders} />}
          <div className='cuadre__info'>
            {cuadre && (
              <>
                <div className='general__card'>
                  <div className='general__card-info'>
                    <h2>{cuadre?.orders?.length || 0}</h2>
                    <p>Facturas</p>
                  </div>
                  <RiNewspaperFill />
                </div>
                <div className='general__card'>
                  <div className='general__card-info'>
                    <h2>${Number(cash).toFixed(2) || 0}</h2>
                    <p>Efectivo</p>
                  </div>
                  <FiUsers />
                </div>
                <div className='general__card'>
                  <div className='general__card-info'>
                    <h2>${Number(card).toFixed(2) || 0}</h2>
                    <p>Tarjeta</p>
                  </div>
                  <FiUsers />
                </div>
                <div className='general__card'>
                  <div className='general__card-info'>
                    <h2>${Number(office).toFixed(2) || 0}</h2>
                    <p>Oficina</p>
                  </div>
                  <FiUsers />
                </div>
              </>
            )}
            <div className='general__card '>
              <div className='general__card-info cashier-input'>
                <FormatNumber
                  values={entryMoney}
                  setValues={setEntryMoney}
                  label={'Monto Recibido'}
                />
                {!cuadre && <p className='cashier-label'>Caja Chica</p>}
              </div>
              {!cuadre && (
                <Button
                  variant='contained'
                  color='success'
                  type='submit'
                  style={{ width: '50%', height: '50px' }}
                  onClick={handleCreateCuadre}
                  disabled={cuadre}
                >
                  Ingresar
                </Button>
              )}
            </div>
          </div>
        </div>
      )}
      <CreateModal
        open={open}
        handleClose={handleClose}
        handlePrint={handlePrintCuadre}
      />
    </div>
  )
}

export default Cuadre
