import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router'

//Utils
import formatDate from '../utils/formatDate'

//Css
import logo from '../images/logo_letter.png'
import '../styles/print.css'

const PrintCuadre = () => {
  const navigate = useNavigate()
  const { printData } = useSelector((state) => state.print)
  const { cuadre } = useSelector((state) => state.cuadre)

  window.onafterprint = function () {
    navigate('/facturation')
  }

  useEffect(() => {
    handlePrint()
  }, [])

  const handlePrint = () => {
    setTimeout(() => {
      window.print()
    }, 1000)
  }

  return (
    <div className='ticket'>
      <img src={logo} alt='Logotipo' />

      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Caja Chica:</p>
        <p>${cuadre.entryMoney}</p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Efectivo sistema:</p>
        <p>
          $
          {printData.items
            .filter((i) => i.paymentMethod === 'Efectivo')
            .reduce((acc, c) => acc + c.totalPrice, 0)}
        </p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Tarjeta sistema:</p>
        <p>
          $
          {printData.items
            .filter((i) => i.paymentMethod === 'Tarjeta')
            .reduce((acc, c) => acc + c.totalPrice, 0)}
        </p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Oficina:</p>
        <p>
          $
          {printData.items
            .filter((i) => i.paymentMethod === 'Oficina')
            .reduce((acc, c) => acc + c.totalPrice, 0)}
        </p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Efectivo Cuadre:</p>
        <p>${printData.cash}</p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Tarjeta Cuadre:</p>
        <p>${printData.card}</p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Notas:</p>
        <p>{printData.notes}</p>
      </div>

      {printData &&
        printData?.items.map((i) => (
          <>
            <table>
              <thead>
                <tr>
                  <th></th>
                  <th>#</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{formatDate(new Date(i.createdAt), 'year')}</td>
                  <td>{i.number}</td>
                  <td>{i.totalPrice}</td>
                </tr>
              </tbody>
            </table>
          </>
        ))}
    </div>
  )
}

export default PrintCuadre
