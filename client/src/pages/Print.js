import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router'

//Utils
import formatDate from '../utils/formatDate'
import convertPrice from '../utils/convertPrice'

//Css
import logo from '../images/logo_letter.png'
import '../styles/print.css'

const Print = () => {
  const navigate = useNavigate()
  const { printData } = useSelector((state) => state.print)
  const { userInfo } = useSelector((state) => state.user)
  const { order } = useSelector((state) => state.createdOrder)

  window.onafterprint = function () {
    navigate('/facturation')
  }
  let date = formatDate(printData.time, 'date')

  useEffect(() => {
    handlePrint()
  }, [])

  const handlePrint = () => {
    setTimeout(() => {
      window.print()
    }, 1000)
  }

  const getTotal = () => {
    if (userInfo.position === 'Hugo & Delivery') return subtotal
    else return subtotal + itbis + tip
  }

  const subtotal = printData.orderItems.reduce(
    (acc, c) => acc + Number(c.price) * c.qty,
    0
  )
  const itbis = subtotal * 0.18
  const tip = subtotal * 0.1
  const total = getTotal()

  return (
    <div className='ticket'>
      <img src={logo} alt='Logotipo' />
      <p className='centrado' style={{ marginBottom: '10px' }}>
        Las Empanadas
        <br />
        Tuki Tuki Investment SRL
        <br />
        RNC 131778798
        <br />
        {date}
      </p>
      {printData && (
        <table>
          <thead>
            <tr>
              <th>CANT</th>
              <th>PRODUCTO</th>
              <th>$</th>
            </tr>
          </thead>
          <tbody>
            {printData.orderItems.map((i) => (
              <tr key={i._id}>
                <td>{i.qty}</td>
                <td>{i.name}</td>
                <td>
                  $
                  {userInfo.position !== 'Hugo & Delivery'
                    ? i.price
                    : convertPrice(i.price, userInfo.position)}
                </td>
              </tr>
            ))}

            <tr className='low-info '>
              <td>-----------</td>
              <td>-----------</td>
              <td>-----------</td>
            </tr>
            {userInfo.position !== 'Hugo & Delivery' && (
              <>
                <tr className='low-info'>
                  <td>SubTotal:</td>
                  <td></td>
                  <td>${Number(subtotal).toFixed(2)}</td>
                </tr>
                <tr className='low-info'>
                  <td>Itbis %18:</td>
                  <td></td>
                  <td>${itbis.toFixed(2)}</td>
                </tr>
                <tr className='low-info'>
                  <td>% Ley:</td>
                  <td></td>
                  <td>${tip.toFixed(2)}</td>
                </tr>
              </>
            )}
            <tr className='low-info '>
              <td>
                <strong>Total:</strong>
              </td>
              <td></td>
              <td>${total.toFixed(2)}</td>
            </tr>
            {printData.paymentMethod === 'Efectivo' && (
              <>
                <tr className='low-info'>
                  <td>Efectivo:</td>
                  <td></td>
                  <td>${printData.cash}</td>
                </tr>
                <tr className='low-info'>
                  <td>Cambio:</td>
                  <td></td>
                  <td>${printData.cash - Math.floor(printData.totalPrice)}</td>
                </tr>
              </>
            )}
            {printData.paymentMethod === 'Oficina' && (
              <>
                <tr className='low-info'>
                  <td>Oficina:</td>
                  <td></td>
                  <td>{printData.office}</td>
                </tr>
              </>
            )}
          </tbody>
        </table>
      )}
      <p className='centrado' style={{ marginTop: '10px' }}>
        Te atendió: {userInfo?.name}
        <br />
        C/ Heriberto nuñez #19
        <br />
        <br />
        ¡GRACIAS POR SU COMPRA!
        <br />
        {printData.number && `#${printData.number}`}
        {order && `#${order.number}`}
      </p>
    </div>
  )
}

export default Print
