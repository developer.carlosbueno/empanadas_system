import { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'

//Actions
import { listProducts } from '../actions/products'

//Components
import SearchInput from '../components/inputs/SearchInput'
import Button from '@mui/material/Button'
import CreateModal from '../components/modals/inventory/CreateModal'

//Css
import '../styles/inventory.css'
import { Divider } from '@mui/material'
import Table from '../components/Table'

const Inventory = () => {
  const dispatch = useDispatch()

  const [search, setSearch] = useState('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  useEffect(() => {
    dispatch(listProducts())
  }, [dispatch])

  return (
    <div className='inventory panel'>
      <h1 className='panel__section'>Inventario</h1>

      <div className='inventory__header'>
        <SearchInput
          value={search}
          setValue={setSearch}
          placeholder='producto...'
        />

        <Button variant='contained' onClick={handleOpen}>
          Crear
        </Button>
      </div>

      <Divider component='div' />
      <Table search={search} />

      {/* Modals */}
      <CreateModal open={open} handleClose={handleClose} />
    </div>
  )
}

export default Inventory
