import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { useSnackbar } from 'notistack'

//Actions
import { createOrder } from '../actions/orders'
import { listProducts } from '../actions/products'
import { getPrintData } from '../actions/print'

//Utils
import convertPrice from '../utils/convertPrice'

//Components
import FacturationProducts from '../components/facturation/FacturationProducts'
import MenuOptions from '../components/facturation/MenuOptions'
import FacturationInfo from '../components/facturation/FacturationInfo'

//Css
import '../styles/facturation/facturationProducts.css'
import '../styles/facturation/menuOptions.css'
import '../styles/facturation/facturationInfo.css'
import '../styles/facturation/facturation.css'

function Facturation() {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { products } = useSelector((state) => state.products)
  const { userInfo } = useSelector((state) => state.user)
  const [selectedProducts, setSelectedProducts] = useState([])
  const [selectedCategory, setSelectedCategory] = useState('empanadas')
  const [paymentMethod, setPaymentMethod] = useState('')
  const [office, setOffice] = useState('')
  const [cash, setCash] = useState('')

  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  useEffect(() => {
    dispatch(listProducts())
  }, [dispatch])

  const getTotal = () => {
    if (userInfo.position === 'Hugo & Delivery') return subtotal
    else return subtotal + itbis + tip
  }

  let subtotal = selectedProducts.reduce((acc, current) => {
    return acc + convertPrice(current.price, userInfo.position) * current.qty
  }, 0)
  let itbis = subtotal * 0.18
  let tip = subtotal * 0.1
  let total = getTotal()
  let itemsQty = selectedProducts.reduce((acc, current) => acc + current.qty, 0)

  const handleDelete = (name) => {
    let exist = selectedProducts.filter((product) => product.name === name)[0]
    let currentItems
    if (exist) {
      if (exist.qty === 1) {
        console.log(exist)
        let items = selectedProducts.filter((item) => item.name !== exist.name)
        currentItems = [...items]
      } else {
        exist.qty = exist.qty - 1
        currentItems = [...selectedProducts]
      }
    } else {
      currentItems = [...selectedProducts]
    }
    setSelectedProducts(currentItems)
  }

  const handleAddProduct = (name, price, qty, id) => {
    let exist = selectedProducts.filter((product) => product.name === name)[0]
    let currentItems
    if (exist) {
      exist.qty = exist.qty + 1
      currentItems = [...selectedProducts]
    } else {
      currentItems = [...selectedProducts, { name, price, qty, product: id }]
    }
    setSelectedProducts(currentItems)
  }

  const handleCreateOrder = (e) => {
    e.preventDefault()
    if (window.confirm('Estas seguro de realizar esta orden?')) {
      if (userInfo.position !== 'Hugo & Delivery') {
        if (selectedProducts.length > 0 && paymentMethod !== '') {
          dispatch(
            createOrder({
              orderItems: selectedProducts,
              totalPrice: total.toFixed(2),
              paymentMethod,
            })
          )
          if (paymentMethod === 'Efectivo') {
            dispatch(
              getPrintData({
                orderItems: selectedProducts,
                totalPrice: total.toFixed(2),
                itbis: itbis.toFixed(2),
                tip: tip.toFixed(2),
                time: new Date(),
                paymentMethod,
                cash,
              })
            )
          } else if (paymentMethod === 'Oficina') {
            dispatch(
              getPrintData({
                orderItems: selectedProducts,
                totalPrice: total.toFixed(2),
                itbis: itbis.toFixed(2),
                tip: tip.toFixed(2),
                time: new Date(),
                paymentMethod,
                office,
              })
            )
          } else {
            dispatch(
              getPrintData({
                orderItems: selectedProducts,
                totalPrice: total.toFixed(2),
                itbis: itbis.toFixed(2),
                tip: tip.toFixed(2),
                time: new Date(),
              })
            )
          }
          navigate('/print')
        }
        if (selectedProducts.length <= 0)
          enqueueSnackbar('Agregue articulos para facturar', {
            variant: 'error',
          })
        if (paymentMethod === '')
          enqueueSnackbar('Eliga un metodo de pago', { variant: 'error' })
      }
      if (userInfo.position === 'Hugo & Delivery') {
        if (selectedProducts.length > 0) {
          dispatch(
            createOrder({
              orderItems: selectedProducts,
              totalPrice: total.toFixed(2),
              paymentMethod: 'Tarjeta',
            })
          )
          dispatch(
            getPrintData({
              orderItems: selectedProducts,
              totalPrice: total.toFixed(2),
              itbis: itbis.toFixed(2),
              tip: tip.toFixed(2),
              time: new Date(),
            })
          )
          navigate('/print')
        }
        if (selectedProducts.length <= 0)
          enqueueSnackbar('Agregue articulos para facturar', {
            variant: 'error',
          })
      }
    }
  }

  return (
    <div className='panel'>
      <h1 className='panel__section'>Facturacion</h1>
      {products && (
        <div className='facturation'>
          <div className='facturation__table'>
            <MenuOptions
              selectedCategory={selectedCategory}
              setSelectedCategory={setSelectedCategory}
            />
            <FacturationProducts
              products={products}
              handleAddProduct={handleAddProduct}
              selectedCategory={selectedCategory}
            />
          </div>
          <FacturationInfo
            subtotal={subtotal}
            itbis={itbis}
            tip={tip}
            total={total}
            itemsQty={itemsQty}
            paymentMethod={paymentMethod}
            setPaymentMethod={setPaymentMethod}
            handleCreateOrder={handleCreateOrder}
            seletedProducts={selectedProducts}
            selectedCategory={selectedCategory}
            handleDelete={handleDelete}
            setOffice={setOffice}
            cash={cash}
            setCash={setCash}
          />
        </div>
      )}
    </div>
  )
}

export default Facturation
