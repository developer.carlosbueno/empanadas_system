import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { useSnackbar } from 'notistack'

//Actions
import { loginUser } from '../actions/users'

//Components
import {
  Button,
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
} from '@mui/material'
import { MdAccountCircle } from 'react-icons/md'
import { RiLockPasswordFill } from 'react-icons/ri'

//CSS
import '../styles/login.css'

const Login = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const { userInfo, error, loading } = useSelector((state) => state.user)

  useEffect(() => {
    if (userInfo) {
      enqueueSnackbar('has iniciado sesion', { variant: 'success' })
      navigate('/facturation')
    }
    if (error) {
      enqueueSnackbar(error, { variant: 'error' })
    }
  }, [dispatch, navigate, userInfo, error])

  const handleSubmit = (e) => {
    e.preventDefault()
    dispatch(loginUser(username, password))
  }

  return (
    <>
      <div className='auth'>
        <form className='form'>
          <h2 style={{ marginBottom: '20px' }}>Iniciar Sesion</h2>
          <FormControl variant='standard' style={{ marginBottom: '20px' }}>
            <InputLabel htmlFor='input-with-icon-adornment'>
              Username
            </InputLabel>
            <Input
              id='input-with-icon-adornment'
              type='text'
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              name='username'
              startAdornment={
                <InputAdornment position='start'>
                  <MdAccountCircle size='30px' />
                </InputAdornment>
              }
            />
          </FormControl>
          <FormControl variant='standard' style={{ marginBottom: '30px' }}>
            <InputLabel htmlFor='input-with-icon-adornment'>
              Password
            </InputLabel>
            <Input
              id='input-with-icon-adornment'
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              type='password'
              startAdornment={
                <InputAdornment position='start'>
                  <RiLockPasswordFill size='30px' />
                </InputAdornment>
              }
            />
          </FormControl>
          <Button
            variant='outlined'
            type='submit'
            disabled={loading}
            onClick={handleSubmit}
          >
            Iniciar
          </Button>
        </form>
      </div>
    </>
  )
}

export default Login
