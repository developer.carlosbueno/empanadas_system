import { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'

//Actions
import { listUsers } from '../actions/users'

//Components
import SearchInput from '../components/inputs/SearchInput'
import Button from '@mui/material/Button'
import CreateEmployeeModal from '../components/modals/employee/CreateModal'
import EmployeesTable from '../components/tables/EmployeesTable'

//Css
import '../styles/inventory.css'
import { Divider } from '@mui/material'

const Employees = () => {
  const dispatch = useDispatch()

  const [search, setSearch] = useState('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  useEffect(() => {
    dispatch(listUsers())
  }, [dispatch])

  return (
    <div className='inventory panel'>
      <h1 className='panel__section'>Empleados</h1>

      <div className='inventory__header'>
        <SearchInput
          value={search}
          setValue={setSearch}
          placeholder='empleado...'
        />

        <Button variant='contained' onClick={handleOpen}>
          Crear
        </Button>
      </div>

      <Divider component='div' />
      <EmployeesTable search={search} />

      {/* Modals */}
      <CreateEmployeeModal open={open} handleClose={handleClose} />
    </div>
  )
}

export default Employees
