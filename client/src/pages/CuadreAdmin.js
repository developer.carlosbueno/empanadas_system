import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

//Actions
import { listTodayCuadres } from '../actions/cuadre'
import { listUsers } from '../actions/users'

//Date
// import AdapterDateFns from '@mui/lab/AdapterDateFns'
// import LocalizationProvider from '@mui/lab/LocalizationProvider'
// import DesktopDatePicker from '@mui/lab/DesktopDatePicker'

//Components
import { FiUsers } from 'react-icons/fi'
import { RiNewspaperFill } from 'react-icons/ri'
import ExpandTable from '../components/tables/ExpandTableAdmin'

//Css
import '../styles/cuadre.css'

const CuadreAdmin = () => {
  const dispatch = useDispatch()

  const { cuadres } = useSelector((state) => state.cuadres)
  const { users } = useSelector((state) => state.userList)

  // const [value, setValue] = useState(new Date('2014-08-18T21:11:54'))

  // const handleChange = (newValue) => {
  //   setValue(newValue)
  // }

  let userWork = users?.filter((i) => i.position === 'Cajera')

  let items = []

  cuadres?.forEach((i) => {
    userWork?.filter((u) => u._id === i.user._id && items.push(i))
  })

  let cash
  items.forEach((i) => {
    cash = i.orders
      .filter((i) => i.paymentMethod === 'Efectivo')
      .reduce((acc, i) => acc + i.totalPrice, 0)
  })

  let card
  items.forEach((i) => {
    card = i.orders
      .filter((i) => i.paymentMethod === 'Tarjeta')
      .reduce((acc, i) => acc + i.totalPrice, 0)
  })

  useEffect(() => {
    dispatch(listTodayCuadres())
    dispatch(listUsers())
  }, [dispatch])

  return (
    <div className='panel'>
      <h1 className='panel__section'>Cuadre general</h1>
      {/* <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DesktopDatePicker
          label='Date desktop'
          inputFormat='MM/dd/yyyy'
          value={value}
          onChange={handleChange}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider> */}
      <div className='cuadre'>
        {items && <ExpandTable info={items} users={userWork} />}
        <div className='cuadre__info'>
          <div className='general__card'>
            <div className='general__card-info'>
              <h2>{items?.length || 0}</h2>
              <p>Facturas</p>
            </div>
            <RiNewspaperFill />
          </div>
          <div className='general__card'>
            <div className='general__card-info'>
              <h2>${cash || 0}</h2>
              <p>Efectivo</p>
            </div>
            <FiUsers />
          </div>
          <div className='general__card'>
            <div className='general__card-info'>
              <h2>${card || 0}</h2>
              <p>Tarjeta</p>
            </div>
            <FiUsers />
          </div>
        </div>
      </div>
    </div>
  )
}

export default CuadreAdmin
