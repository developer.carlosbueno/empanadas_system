import { actions } from '../actions/orders'

export const orderReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.ORDER_CREATE_REQUEST:
      return {
        loading: true,
      }

    case actions.ORDER_CREATE_SUCCESS:
      return {
        loading: false,
        success: true,
        order: payload,
      }

    case actions.ORDER_CREATE_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const orderDetailReducer = (
  state = {
    loading: true,
  },
  { type, payload }
) => {
  switch (type) {
    case actions.ORDER_DETAILS_REQUEST:
      return {
        ...state,
        loading: true,
      }

    case actions.ORDER_DETAILS_SUCCESS:
      return {
        loading: false,
        order: payload,
      }

    case actions.ORDER_DETAILS_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const orderMyListReducer = (
  state = { orders: [] },
  { type, payload }
) => {
  switch (type) {
    case actions.ORDER_MY_LIST_REQUEST:
      return {
        loading: true,
      }

    case actions.ORDER_MY_LIST_SUCCESS:
      return {
        loading: false,
        orders: payload,
      }

    case actions.ORDER_MY_LIST_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const orderListReducer = (state = { orders: [] }, { type, payload }) => {
  switch (type) {
    case actions.ORDER_ALL_REQUEST:
      return {
        loading: true,
      }

    case actions.ORDER_ALL_SUCCESS:
      return {
        loading: false,
        orders: payload,
      }

    case actions.ORDER_ALL_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}
