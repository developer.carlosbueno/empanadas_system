import { actions } from '../actions/users'

export const userLoginReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.USER_LOGIN_REQUEST:
      return { loading: true }

    case actions.USER_LOGIN_SUCCESS:
      return { loading: false, userInfo: payload }

    case actions.USER_LOGIN_FAIL:
      return { error: payload }

    case actions.USER_LOGOUT:
      return {}

    default:
      return state
  }
}

export const userRegisterReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.USER_REGISTER_REQUEST:
      return { loading: true }

    case actions.USER_REGISTER_SUCCESS:
      return { loading: false, userInfo: payload }

    case actions.USER_REGISTER_FAIL:
      return { loading: false, error: payload }

    default:
      return state
  }
}

export const userDetailsReducer = (state = { user: [] }, { type, payload }) => {
  switch (type) {
    case actions.USER_DETAILS_REQUEST:
      return {
        loading: true,
      }

    case actions.USER_DETAILS_SUCCESS:
      return {
        loading: false,
        user: payload,
      }

    case actions.USER_DETAILS_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const userListReducer = (state = { users: [] }, { type, payload }) => {
  switch (type) {
    case actions.USER_LIST_REQUEST:
      return {
        loading: true,
      }

    case actions.USER_LIST_SUCCESS:
      return {
        loading: false,
        users: payload,
      }

    case actions.USER_LIST_FAIL:
      return {
        loading: false,
        error: payload,
      }

    case actions.USER_LIST_RESET:
      return { users: [] }

    default:
      return state
  }
}

export const userUpdateReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.USER_UPDATE_REQUEST:
      return {
        loading: true,
      }

    case actions.USER_UPDATE_SUCCESS:
      return {
        loading: false,
        user: payload,
      }

    case actions.USER_UPDATE_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}
