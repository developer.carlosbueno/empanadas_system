import { actions } from '../actions/products'

export const listProductReducer = (
  state = { products: [] },
  { type, payload }
) => {
  switch (type) {
    case actions.PRODUCT_LIST_REQUEST:
      return {
        ...state,
        loading: true,
      }

    case actions.PRODUCT_LIST_SUCCESS:
      return {
        loading: false,
        products: payload,
      }

    case actions.PRODUCT_LIST_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const detailProductReducer = (
  state = { product: {} },
  { type, payload }
) => {
  switch (type) {
    case actions.PRODUCT_DETAIL_REQUEST:
      return {
        ...state,
        loading: true,
      }

    case actions.PRODUCT_DETAIL_SUCCESS:
      return {
        loading: false,
        product: payload,
      }

    case actions.PRODUCT_DETAIL_FAIL:
      return {
        loading: false,
        error: payload,
      }

    case actions.PRODUCT_DETAIL_RESET:
      return {}

    default:
      return state
  }
}

export const createProductReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.PRODUCT_CREATE_REQUEST:
      return {
        loading: true,
      }

    case actions.PRODUCT_CREATE_SUCCESS:
      return {
        loading: false,
        product: payload,
      }

    case actions.PRODUCT_CREATE_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const updateProductReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.PRODUCT_UPDATE_REQUEST:
      return {
        loading: true,
      }

    case actions.PRODUCT_UPDATE_SUCCESS:
      return {
        loading: false,
        product: payload,
      }

    case actions.PRODUCT_UPDATE_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}
