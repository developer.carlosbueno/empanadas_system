import { actions } from '../actions/cuadre'

export const cuadreReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.CUADRE_CREATE_REQUEST:
      return {
        loading: true,
      }

    case actions.CUADRE_CREATE_SUCCESS:
      return {
        loading: false,
        cuadre: payload,
      }
    case actions.CUADRE_CREATE_FAIL:
      return {
        loading: false,
        error: payload,
      }

    case actions.CUADRE_MY_REQUEST:
      return {
        loading: true,
      }

    case actions.CUADRE_MY_SUCCESS:
      return {
        loading: false,
        cuadre: payload,
      }
    case actions.CUADRE_MY_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}

export const cuadresReducer = (state = [], { type, payload }) => {
  switch (type) {
    case actions.CUADRE_TODAY_REQUEST:
      return {
        loading: true,
      }

    case actions.CUADRE_TODAY_SUCCESS:
      return {
        loading: false,
        cuadres: payload,
      }
    case actions.CUADRE_TODAY_FAIL:
      return {
        loading: false,
        error: payload,
      }

    default:
      return state
  }
}
