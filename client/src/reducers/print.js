import { actions } from '../actions/print'

export const printReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case actions.GET_PRINT_DATA:
      return {
        printData: payload,
      }

    case actions.FORMAT_PRINT_DATA:
      return {}

    default:
      return state
  }
}
