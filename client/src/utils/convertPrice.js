export default function convertPrice(p, position) {
  if (position === 'Hugo & Delivery') {
    switch (p) {
      case 46.875:
        return 60.0
      case 39.065:
        return 50.0
      case 66.8:
        return 1.0
      case 97.66:
        return 150.0
      case 117.19:
        return 175.0
      case 136.72:
        return 200.0
      case 156.25:
        return 250.0
      default:
        break
    }
  } else return p
}
