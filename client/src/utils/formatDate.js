export default function formatDate(date, type) {
  let result
  switch (type) {
    case 'date':
      result = new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
      }).format(date)
      return result
    case 'time':
      result = new Intl.DateTimeFormat('en-US', {
        hour: '2-digit',
        minute: '2-digit',
      }).format(date)
      return result
    case 'year':
      result = new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
      }).format(date)
      return result

    default:
      return result
  }
}
