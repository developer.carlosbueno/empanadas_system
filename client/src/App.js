import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Layout from './components/Layout'

//Components
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'
import Inventory from './pages/Inventory'
import Facturation from './pages/Facturation'
import Cuadre from './pages/Cuadre'
import Print from './pages/Print'
import PrintCuadre from './pages/PrintCuadre'
import Employees from './pages/Employees'
import CuadreAdmin from './pages/CuadreAdmin'

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path='/login' element={<Login />} />
          <Route path='/facturation' element={<Facturation />} />
          <Route path='/cuadre' element={<Cuadre />} />
          <Route path='/print' element={<Print />} />
          <Route path='/print_cuadre' element={<PrintCuadre />} />
          <Route path='/admin/dashboard' element={<Dashboard />} />
          <Route path='/admin/inventory' element={<Inventory />} />
          <Route path='/admin/employees' element={<Employees />} />
          <Route path='/admin/facturas' element={<CuadreAdmin />} />
        </Routes>
      </Layout>
    </Router>
  )
}

export default App
