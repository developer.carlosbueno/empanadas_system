const express = require('express')
const cors = require('cors')
const app = express()

//DB
require('./db')

//Routes
const userRoutes = require('./routes/user')
const productRoutes = require('./routes/products')
const orderRoutes = require('./routes/order')
const cuadreRoutes = require('./routes/cuadre')

//Settings
const path = require('path')
const { server } = require('./config')
app.set('views', path.join(__dirname, 'client/build'))

//Middleawares
app.use(cors())
app.use(express.json())

//Router
app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)
app.use('/api/orders', orderRoutes)
app.use('/api/cuadres', cuadreRoutes)

//Static files

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'))
}

//Initialization

app.listen(server.port, () => console.log(`server on port ${server.port}`))
