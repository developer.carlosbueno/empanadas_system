const jwt = require('jsonwebtoken')
const User = require('../models/user')
const { fail } = require('../utils/response')

const protect = async (req, res, next) => {
  let token
  let decoded
  let authorization = req.headers.authorization
  try {
    if (authorization && authorization.startsWith('Bearer')) {
      token = authorization.split(' ')[1]
      decoded = await decodeToken(token)
      req.user = await User.findById(decoded.id).select('-password')
      next()
    } else if (!token) {
      fail(res, 401, 'No authorization, no token')
    } else {
      fail(res, 401, 'No authorize')
    }
  } catch (error) {
    fail(res, 400, error.message)
  }
}

const admin = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next()
  } else {
    fail(res, 401, 'Not admin token')
  }
}

const decodeToken = (token) => {
  return jwt.verify(token, 'elpepe')
}

module.exports = {
  protect,
  admin,
}
