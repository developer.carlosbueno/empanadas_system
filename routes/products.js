const router = require('express').Router()
const {
  getAll,
  getOne,
  insert,
  updateProduct,
  deleteProduct,
} = require('../controllers/product')
const { admin } = require('../middlewares/authMiddleware')

router.get('/', getAll)
router.get('/:id', getOne)
router.post('/', insert)
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)

module.exports = router
