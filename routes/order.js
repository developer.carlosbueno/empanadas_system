const router = require('express').Router()
const {
  createOrder,
  getOrders,
  getMyOrders,
  getOrdersFromToday,
} = require('../controllers/order')
const { protect, admin } = require('../middlewares/authMiddleware')

router.post('/', protect, createOrder)
router.get('/', getOrders)
router.get('/today', protect, admin, getOrdersFromToday)
router.get('/:id', protect, getMyOrders)

module.exports = router
