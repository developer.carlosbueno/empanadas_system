const router = require('express').Router()
const {
  createCuadre,
  getCuadres,
  getMyCuadre,
  updateCuadre,
  getCuadresFromToday,
} = require('../controllers/cuadre')
const { protect, admin } = require('../middlewares/authMiddleware')

router.post('/', protect, createCuadre)
router.get('/', protect, admin, getCuadres)
router.get('/today', protect, admin, getCuadresFromToday)
router.get('/:id', protect, getMyCuadre)
router.put('/:id', protect, updateCuadre)

module.exports = router
