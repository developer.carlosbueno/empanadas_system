const router = require('express').Router()
const { login, register, getUsers, updateUser } = require('../controllers/user')
const { admin, protect } = require('../middlewares/authMiddleware')

router.post('/login', login)
router.post('/register', protect, admin, register)
router.get('/', protect, admin, getUsers)
router.put('/:id', protect, admin, updateUser)

module.exports = router
